var objPartida = {  //var, porque es la unica que se puede usar interventanas (de esta a la de nueva partida)
    iniciada: false,
    saldo: 3000,
    recaudacion: 0,
    visitantes: 0,
    parque: [],
    hora: new Date
};



// Ejecución paneles
document.getElementById('nuevaPartida').onclick = function (){ 

    if (!objPartida.iniciada) {

        open("paneles/nuevapartida.html", 'Nueva partida', 'scrollbars=yes,width=800,height=900,toolbar=yes');

    } else {

        msg('error', 'Ya has iniciado una partida previamente, no es posible crear una nueva partida');       

    }
}

//panel construccion
const celdas= document.getElementsByClassName('celda');

for(let element of celdas){ 


    element.onclick= function(){ 

        if (objPartida.iniciada){ 

            let ventana= open("paneles/nuevoEdificio.html", "Construir", "width= 800,height=900, toolbar= no, scrollbars= yes")
            ventana.onload = function(){ //evento para determinar que cuando la pagina este cargada ocurra lo otro, cambiar celda
                ventana.document.getElementById('numeroCelda').textContent= element.dataset.celda //cambiar contenido de numero de celda                
            }
            //dataset= se usa elemento porque el onclick se hace sobre el elemento y elemento es un objeto ??y con 
            //dataset es un subobjeto que contiene los atributos data

        } else {

            msg('error', 'Tienes que iniciar una partida para poder construir');

        }
    }
}

document.getElementById("recaudarCaja").onclick=function(){


    if (objPartida.iniciada){

        if(objPartida.recaudacion >= 200){

         open("paneles/recaudarEntradas.html", "Entradas", "width= 750, height= 800, toolbar= no, scrollbars= yes")

            
        }else{

            msg('error', 'no hay suficiente dinero recaudado'); 
        }
        
    }else{

        msg('error', 'Imposible recaudar entradas : inicia una partida antes')
    }

}


document.getElementById('nuevoSorteo').onclick= function(){

    if (!objPartida.iniciada){

        msg('error','Necesitas iniciar sesion para poder participar en el sorteo');
    
    }else{

        if(objPartida.parque.length < 2){

        alert("Necesitas al menos dos edificios para participar en el sorteo");

        }else{
            
            let sorteo= open("paneles/nuevoSorteo.html", "Sorteo", "width= 800,height=900, toolbar= no, scrollbars= yes")

            
        }
         
    }
}





// intervalo de actualización; cada cuanto tiempo se actualiza la partida
setInterval( function(){    

    if (objPartida.iniciada) {


        for (let elemento of objPartida.parque){

            if (elemento.tipo === 'atraccion') {  // calculo numero 

                objPartida.visitantes+= Number(elemento.visitantes)
                objPartida.recaudacion+= elemento.visitantes* 2 
            
            }

            if (elemento.tipo === 'puesto'){
                objPartida.saldo+= Number(elemento.ingresos)
            }
        }

        document.getElementById('contadorEdificios').textContent = objPartida.parque.length + " edificios";
        document.getElementById('contadorVisitantes').textContent = objPartida.visitantes + " visitantes";
        document.getElementById('contadorRecaudacion').textContent = objPartida.recaudacion + " $ en caja";
        document.getElementById('contadorSaldoActual').textContent = objPartida.saldo + " $";

    }

}, 200); 