if(!opener){
    alert("ACCESO DENEGADO");
    location.assign('../index.html'); 
}

//Generar los números aleatorios
let premio= Math.round(Math.random()*4 + 1);

//console.log("El premio se encuentra tras la casilla: " + premio);

let terremoto= premio;

while(terremoto=== premio){ 
	
	terremoto= Math.round(Math.random()*4 +1);
}
//console.log("El terremoto se encuentra tras la casilla: " + terremoto);


//Comprobar si se ha acertado y que se ha gastado el intento de participar
let intento= 0;
const botones= document.getElementsByTagName('button');

 for (let elemento of botones){

	elemento.onclick= function(){
		
		if (elemento.innerText == premio  &&  intento < 1 ){
			opener.objPartida.saldo+= 10000
			msg("success", "Enhorabuena!Ganaste 10000$!")
			intento+=1;

		}else if(elemento.innerText == terremoto  &&  intento < 1 ){
			opener.objPartida.parque.length-= 2 
			const celdas= opener.document.getElementsByClassName('celda');
			let borrados = 0;

			for (let elemento of celdas){
				if (elemento.dataset.edificio != "vacia"){
					elemento.dataset.edificio = "vacia";
					borrados += 1;
				}
				if (borrados == 2){
					break;
				}
			}
			msg('error', 'Terremoto!!! Acabas de perder dos edificios');
			intento+= 1;


		}else if(elemento.innerText != premio  &&  elemento.innerText != terremoto  &&  intento < 1){
			msg("error", "Sigue intentándolo");
			intento+=1;
			
		}else{
			msg("error", "Ya has participado en el sorteo")
		}

	}
}
	

// Extra, un sorteo por hora
function horaActualizada(){

	let horaActual= opener.objPartida.hora.getHours()
	let minutosActuales= opener.objPartida.hora.getMinutes();
	let segundosActuales= opener.objPartida.hora.getSeconds();

	let nuevaHora= horaActual + 1;

	if(horaActual < 10) { horaActual = '0' + horaActual; }
	if(minutosActuales < 10) { minutosActuales = '0' + minutosActuales; }
	if(segundosActuales< 10) { segundosActuales = '0' + segundosActuales; }	

	if(nuevaHora < 10) { nuevaHora = '0' + nuevaHora; }
		
	let reloj= horaActual+ ":" + minutosActuales + ":" + segundosActuales
		
	let nuevoReloj= nuevaHora + ":" + minutosActuales + ":" + segundosActuales
	
	if(nuevoReloj - reloj < 1 ){
		msg("error", "Sólo puedes realizar el sorteo una vez cada hora");
	}
}
			
setInterval(horaActualizada, 1000);
		




