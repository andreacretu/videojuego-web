if(!opener){
    alert("ACCESO DENEGADO");
    location.assign('../index.html');  // cuando se intenta abrir el enlace desde la carpeta, acceso denegado y 
    // envia al usuario a la pagina principal(el index) mediante el metodo assign. el onjeto location detecta donde esta el usario
    // los dos puntos son para decirle que vaya atras
}

// para construir (nº celda, nombre, y los ingresos)

class Edificio{
	constructor(numCelda, nombre){
		this._celda= numCelda;
		this._nombre =nombre;

	}

	inicializar(){
		const celdas= opener.document.getElementsByClassName('celda');
		for (let elemento of celdas){

			if (elemento.dataset.celda== this._celda){  //el for recorre las celdas de que hemos construido y cuando coincide con la
				// del ususaario, tenemos la celda
				elemento.dataset.edificio= this._nombre

			}
		}
	}
}

 class Atraccion extends Edificio{     //herencia, tiene lo mismo que edificio pero ademas se le añade el nº visitantes
 	constructor(numCelda, nombre, visitantes){
 		super (numCelda, nombre); //el super se lo manda al padre
 		this._visitantes = visitantes;
 	}
 	get tipo(){
 		return 'atraccion'
 	}
 	get visitantes(){
 		return this._visitantes
 	}
 }


 class Puesto extends Edificio{
 	constructor (numCelda, nombre, ingresos){
 		super(numCelda, nombre);
 		this._ingresos= ingresos;

 	}
 	get tipo(){
 		return 'puesto'
 	}
 	get ingresos(){
 		return this._ingresos
 	}
 }


let edificios= document.getElementsByClassName('edificio')

for (let elemento of edificios){

	elemento.onclick = function(){

		const numCelda = document.getElementById('numeroCelda').textContent;
		const nombre = elemento.dataset.nombre; //viene de datanombre
		const coste = elemento.dataset.coste; //viene de datanombre
		const tipo = elemento.dataset.tipo; //viene de datanombre

		if (opener.objPartida.saldo < coste){

			msg('error', 'No tienes saldo suficiente')
			
		} else{

			opener.objPartida.saldo -= coste
			
			if (tipo==='atraccion'){

				const visitantes= elemento.dataset.visitantes  //porque si ha hecho click sobre una traccion tiene ya visitantes
				const atraccion= new Atraccion(numCelda, nombre, visitantes);
				atraccion.inicializar();
				opener.objPartida.parque.push(atraccion);
			}

			if (tipo==='puesto'){

				const ingresos= elemento.dataset.ingresos; //porque si ha hecho click sobre una traccion tiene ya visitantes
				const puesto= new Puesto(numCelda, nombre, ingresos);
				puesto.inicializar();
				opener.objPartida.parque.push(puesto);
			}

			opener.msg("success", "Edificio creado!")
			window.close();	
		}
	}
}


//intervalo de aceleracion, cada x milisegundos se repite algo